let axios = require('axios');

const uid = localStorage.getItem('uid');
const client = localStorage.getItem('client');
const accesstoken = localStorage.getItem('accesstoken');
// baseURL: "https://api.itsmycar.fr",

let axiosClient = axios.create({
  baseURL: "http://localhost:3000",
  headers: {
    uid,
    client,
    accesstoken
  }
});

//51.15.155.215
export default axiosClient;

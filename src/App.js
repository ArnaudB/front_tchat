import React, { Component } from 'react';
import axiosClient from './axiosClient';
var Cable = require('actioncable')
class App extends Component {
  constructor(props) {
  super(props);
  this.state = {
    currentChatMessage: '',
    chatLogs: []
    };
  }

  componentWillMount() {
    this.createSocket();
    axiosClient['get']('http://localhost:3000/api/v1/super_admin/chat_rooms/1/messages')
    .then(response => {
      this.setState({ chatLogs: response.data })
      });
  }

  updateCurrentChatMessage(event) {
    this.setState({
      currentChatMessage: event.target.value
    });
  }

  render() {
    return (
      <div className='App'>
        <div className='stage'>
          <h1>Chat</h1>
          <div className='chat-logs'>
          </div>
          <input
            onKeyPress={ (e) => this.handleChatInputKeyPress(e) }
            value={ this.state.currentChatMessage }
            onChange={ (e) => this.updateCurrentChatMessage(e) }
            type='text'
            placeholder='Enter your message...'
            className='chat-input' />
          <button
            onClick={ (e) => this.handleSendEvent(e) }
            className='send'>
            Send
          </button>
        </div>
        <ul className='chat-logs'>
          { this.renderChatLog() }
        </ul>
      </div>
    );
  }

  handleChatInputKeyPress(event) {
    if(event.key === 'Enter') {
      this.handleSendEvent(event);
    }//end if
  }
  
  handleSendEvent(event) {
    event.preventDefault();
    // A REMPLACE PAR: axiosClient['put']('http://localhost:3000/api/v1/super_admin/chat_rooms/1/messages/10'
    // POUR TESTER LUPDATE
    axiosClient['put']('http://localhost:3000/api/v1/super_admin/chat_rooms/1/messages/10', {
        message: {
          content: this.state.currentChatMessage,
          super_admin_id: 3,
          chat_room_id: 1
        }
      })
    this.setState({
      currentChatMessage: ''
    });
  }

  createSocket() {
    let cable = Cable.createConsumer('http://localhost:3000/cable');
    this.chats = cable.subscriptions.create({
      channel: 'ChatRoomsChannel',
      room: 1
    }, {
        received: (data) => {
          console.log(data.messages);
          this.setState({ chatLogs: [] })
          this.setState({ chatLogs: data.messages })
        }
    })
  }

  renderChatLog() {
    return this.state.chatLogs.map((el) => {
      return (
        <li>
          <span className='chat-message'>{ el.content }</span>
        </li>
      );
    });
  }
}


export default App;